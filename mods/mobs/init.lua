
local path = minetest.get_modpath("mobs")


-- Mobs Api

mobs = {}
mobs.mod = "redo"
mobs.version = "20180808"


-- CMI support check
local use_cmi = minetest.global_exists("cmi")


-- Invisibility mod check
mobs.invis = {}
if minetest.global_exists("invisibility") then
	mobs.invis = invisibility
end


-- Mob functions
--dofile(path .. "/functions.lua")

-- Mob API
dofile(path .. "/api.lua")

-- Rideable Mobs
dofile(path .. "/mount.lua")

minetest.log("action", "[MOD] Mobs Redo loaded")
