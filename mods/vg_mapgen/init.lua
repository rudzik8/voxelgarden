dofile(minetest.get_modpath("vg_mapgen").."/mapgen.lua")

if minetest.get_mapgen_setting("mg_name") == "v6" then
	dofile(minetest.get_modpath("vg_mapgen").."/v6-decorations.lua")
else
	dofile(minetest.get_modpath("vg_mapgen").."/biomes.lua")
	dofile(minetest.get_modpath("vg_mapgen").."/decorations.lua")
end

if minetest.settings:get_bool("enable_biome_sky", true) then
	dofile(minetest.get_modpath("vg_mapgen").."/biome_sky.lua")
end
