-- Combine values from ... into one
function table.combine(t, ...)
	for _,s in pairs({...}) do
		for i=1,#s do
			t[#t+1] = s[i]
		end
	end
	return t
end

-- Update all keys in t with values from ...
function table.update(t, ...)
	for _,s in pairs({...}) do
		for k,v in pairs(s) do
			t[k] = v
		end
	end
	return t
end

-- Update all nil keys in t with values from ...
function table.update_nil(t, ...)
	for _,s in pairs({...}) do
		for k,v in pairs(s) do
			if t[k] == nil then
				t[k] = v
			end
		end
	end
	return t
end

-- A pairs() implementation that returns keys sorted
-- f is an alternative order function (optional)
-- <https://www.lua.org/pil/19.3.html>
function table.pairs_by_keys(t, f)
	local a = {}
	for n in pairs(t) do
		a[#a+1] = n
	end

	table.sort(a, f)

	local i = 0
	local function iter()
		i = i + 1
		if a[i] == nil then
			return
		else
			return a[i], t[a[i]]
		end
	end
	return iter
end
pairs_by_keys = table.pairs_by_keys
