# `vg_init`

## License of source code

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

http://www.gnu.org/licenses/lgpl-2.1.html

## License of media

Creative Commons Attribution-ShareAlike 4.0 International \
http://creativecommons.org/licenses/by-sa/4.0/

## Authors of media files

rudzik8 (CC BY-SA 4.0):
* `unknown_*` (based on textures by Gambit)
