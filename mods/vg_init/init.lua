voxelgarden = {
	flavor = "nova",
	-- Possible flavors:
	-- * "nova"
	-- * "classic"

	roll = 7,
	week = "2024-35",
}

function voxelgarden.get_ver()
	return "\"" .. voxelgarden.flavor .. "\"" .. " " ..
		voxelgarden.week ..
		" r" ..
		voxelgarden.roll
end

local modpath = minetest.get_modpath("vg_init")

-- table functions
-- (should be compatible with such from mcl_util)
dofile(modpath .. "/table.lua")

-- misc. utility functions
dofile(modpath .. "/util.lua")
