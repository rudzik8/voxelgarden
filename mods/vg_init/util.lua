-- Call `on_rightclick` of the pointed node if defined.
function voxelgarden.call_on_rightclick(itemstack, player, pointed_thing)
	-- Only proceed if the following conditions are met:
	--  * `pointed_thing` isn't nil;
	--  * `pointed_thing` is a node;
	--  * `player` isn't nil;
	--  * `player` is a valid ObjectRef to a player.
	--  * `player` isn't sneaking (sneak-place feature).
	if (not pointed_thing) or (pointed_thing.type ~= "node")
			or (
				player and player:is_player()
				and player:get_player_control().sneak
			) then
		return
	end

	-- If the pointed node isn't loaded (WTF?), don't proceed either.
	local node = minetest.get_node_or_nil(pointed_thing.under)
	if not node then return end

	-- Finally, call `on_rightclick` if defined.
	-- Return the original itemstack if the pointed node's
	-- `on_rightclick` doesn't return anything.
	local def = minetest.registered_nodes[node.name]
	if def and def.on_rightclick then
		return def.on_rightclick(pointed_thing.under, node, player, itemstack, pointed_thing)
			or itemstack
	end
end
