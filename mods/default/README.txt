Minetest 0.4 mod: default
==========================

License of source code:
-----------------------
Copyright (C) 2011-2012 celeron55, Perttu Ahola <celeron55@gmail.com>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

http://www.gnu.org/licenses/lgpl-2.1.html

License of media (textures and sounds)
--------------------------------------
Attribution-ShareAlike 3.0 Unported (CC BY-SA 3.0)                                 
http://creativecommons.org/licenses/by-sa/3.0/

Authors of source code
----------------------
Originally by celeron55, Perttu Ahola <celeron55@gmail.com> (LGPLv2.1+)
Various Minetest developers and contributors (LGPLv2.1+)

The torch code was derived by sofar from the 'torches' mod by
BlockMen (LGPLv2.1+)

Authors of media files
-----------------------
Everything not listed in here:
Copyright (C) 2010-2012 celeron55, Perttu Ahola <celeron55@gmail.com>

Originating from work by kddekadenz/Dogers:
  default_grass_footstep.{1,2,3}.ogg
  default_dig_crumbly.{1,2}.ogg

Alecia Shepherd (CC BY-SA 4.0):
  mcl_sounds_cloth.{1,2,3,4}.ogg

Cisoun (CC BY-SA 4.0):
  default_dirt.png
  default_sand.png
  default_wood.png
  default_ladder_wood.png
  default_furnace_fire*
  wieldhand.png
  default_bush_sapling.png

VanessaE (CC BY-SA 4.0):
  default_nc_rb.png
  default_torch_animated.png
  default_torch_on_floor_animated.png
  default_torch_on_floor.png
  default_torch.png
  default_torch_on_ceiling.png
  default_torch_on_floor.png
  default_desert_sand.png
  default_desert_stone.png

Calinou (CC BY-SA 4.0):
  default_tool_steelsword.png

Zeg9 (CC BY-SA 3.0):
  default_coal_block.png
  default_gravel.png

kaeza (CC BY-SA 4.0):
  bubble.png
  default_desert_sandstone.png
  default_desert_sandstone_brick.png
  default_desert_sandstone_block.png
  
rudzik8 (CC BY-SA 4.0):
  default_mineral_mese.png
  default_mese_crystal.png
  default_mese_crystal_fragment.png
  default_mese_block.png
  default_meselamp.png (based on texture by Gambit)
  default_mineral_iron.png
  default_mineral_diamond.png
  default_mineral_gold.png
  default_aspen_sapling.png (based on texture from PixelBOX)
  default_bookshelf.png (based on texture by Gambit)
  default_flint.png (based on texture by XSSheep)
  default_obsidian_glass.png (based on texture from PixelBOX)
  default_pine_sapling.png
  default_jungletree_top.png (based on texture by WisdomFire and on texture from PixelBOX)
  default_granite.png
  default_granite_block.png
  default_granite_brick.png
  default_hardened_rock* (based on texture by Rath)
  default_junglewood.png (based on texture by Cisoun)
  default_pine_wood.png (based on texture by Cisoun)
  default_acacia_wood.png (based on texture by Cisoun)
  default_aspen_wood.png (based on texture by Cisoun)
  default_sign_wood.png (based on texture by Gambit)
  default_obsidian_* (based on texture by Gambit)
  default_desert_sandstone* (based on texture by Gambit)
  default_sandstone_* (based on texture by Gambit)
  default_silver_sand.png (based on texture by Cisoun)
  default_silver_sandstone* (based on texture by Gambit)
  default_bookshelf_slot.png -- Derived from a texture by Gambit (CC BY-SA 3.0)
  default_book_written.png (based on textures by Gambit and CloudyProton)
  default_book_written_wield.png (based on textures by Gambit and CloudyProton)
  default_grass_wield.png (based on texture by tobyplowy and texture by Gambit)
  default_papyrus_wield.png (based on texture by XSSheep)
  default_pine_tree* (based on texture by Gambit)
  default_aspen_leaves.png (based on texture by XSSheep)
  default_brick.png (based on texture by Gambit)
  default_sulphur.png (based on texture by Gambit)
  default_iron_ingot.png (based on texture by Casimir)
  default_iron_block.png (based on texture by kilbith)
  default_chest_lock.png (based on texture by Gambit)
  default_marram_grass_*.png -- Derived from textures by paramat, Cisoun and TumeniNodes (CC BY-SA 4.0)
  default_cactus_bottom.png (based on texture by Gambit)
  default_gold_lump.png
  default_gold_ingot.png (based on texture by Casimir)
  default_gold_block.png (based on texture by kilbith)
  default_tool_copperaxe.png (based on texture by Casimir)
  default_tool_steelaxe.png (based on texture by BlockMen)
  default_tool_stoneaxe.png (based on texture by BlockMen)
  default_tool_woodaxe.png (based on texture by BlockMen)
  default_tool_diamondaxe.png (based on texture by BlockMen)
  default_tool_meseaxe.png (based on texture by BlockMen)
  default_water.png (based on texture by Lopano)
  default_water_source_animated.png (based on texture by Lopano)
  default_water_flowing_animated.png (based on texture by Lopano)

Casimir (CC BY-SA 4.0)
  default_junglesapling.png
  default_jungleleaves.png
  default_mineral_coal.png
  default_coal_lump.png
  default_iron_lump.png
  default_steel_ingot.png (outline by rudzik8)
  default_mineral_copper.png
  default_copper_lump.png
  default_copper_ingot.png (outline by rudzik8)
  default_copper_block.png
  default_glass.png
  default_furnace*
  default_tool_copperpick.png
  default_tool_coppershovel.png
  default_tool_coppersword.png
  heart.png
  default_sapling.png
  default_paper.png
  default_desert_stone.png
  default_stone.png
  crack_anylenght.png
  default_dry_shrub.png
  default_nc_back.png
  default_nc_front.png
  default_nc_side.png
  default_apple*.png
  default_fence.png
  default_torch_on_ceiling_animated.png
  gui_hotbar*
  default_cactus_fig.png (based on work by toby109tt)
  default_seaweed_top.png (based on work by Gambit)
  default_coral_purple.png (based on work by Pithydon)
  default_tin_block.png
  default_tin_ingot.png (outline by rudzik8)
  default_tin_lump.png
  default_mineral_tin.png
  default_bronze_ingot.png (outline by rudzik8)
  default_stone_crumbled.png (based on work by Gambit)
  default_bonfire.png (Based on PixelBOX)
  default_bone.png
  default_seedling
  default_dry_grass_* (based on work by Gambit)
  default_acacia_sapling.png
  default_clay_brick.png (outline by rudzik8)
  default_papyrus_roots.png (based on texture by XSSheep)

Gambit (CC BY-SA 4.0)
  default_book.png
  default_book_wield.png
  default_cactus_side.png
  default_cactus_bottom.png
  default_grass_*
  default_junglegrass.png
  default_sign_wall_wood.png
  default_snow.png
  default_snowball.png
  default_snow_side.png
  default_tree.png
  default_tree_top.png
  default_chest_front.png
  default_chest_side.png
  default_chest_top.png
  default_lava.png
  default_lava_flowing_animated.png
  default_lava_source_animated.png
  default_stone_brick.png
  default_seaweed.png
  default_seaweed_wield.png
  default_sandstone.png
  default_aspen_tree.png
  default_aspen_tree_top.png
  default_bonfire_animated.png
  default_flint.png
  default_acacia_tree*
  default_acacia_leaves.png
  default_pine_needles.png
  default_cloud.png
  default_obsidian.png
  default_obsidian_shard.png
  default_ladder_steel.png

BlockMen (CC BY 3.0)
  default_tool_steelpick.png
  default_tool_steelshovel.png
  default_tool_stonepick.png
  default_tool_stoneshovel.png
  default_tool_woodpick.png
  default_tool_woodshovel.png
  default_tool_diamondpick.png
  default_tool_diamondshovel.png
  default_tool_diamondsword.png
  default_tool_mesepick.png
  default_tool_meseshovel.png
  default_tool_mesesword.png
  default_stick.png

WisdomFire (CC BY-SA 3.0)
  default_jungletree.png

PilzAdam (CC BY-SA 3.0)
  default_ice.png

kilbith (CC BY-SA 3.0)
  default_steel_block.png
  default_bronze_block.png
  default_cobble.png
  default_mossycobble.png

XSSheep's MC PixelPerfection texture pack, converted by sofar (CC BY-SA 4.0)
  default_leaves.png
  default_papyrus.png

tobyplowy (aka toby109tt) (CC BY 3.0)
  default_grass.png
  default_grass_side.png
  default_grass_footsteps.png
  default_cactus_fig_item.png
  default_clay.png
  default_clay_lump.png
  default_kelp.png

Rath (CC BY-SA 3.0)
  default_molten_rock*

Pithydon (CC BY-SA 3.0)
  default_coral_brown.png
  default_coral_orange.png
  default_coral_skeleton.png

Mossmanikin (CC BY-SA 3.0):
  default_fern_*.png
  default_spearstone.png
  default_spearwood.png

Topywo (CC BY-SA 3.0)
  default_coral_cyan.png
  default_coral_green.png
  default_coral_pink.png

Extex101 (CC BY-SA 3.0)
  default_large_cactus_seedling.png

sofar (CC BY-SA 3.0):
  default_aspen_sapling.png

paramat (CC BY-SA 3.0):
  default_permafrost.png -- Derived from a texture by Neuromancer (CC BY-SA 3.0)
  default_moss.png
  default_moss_side.png
  default_pinetree.png
  default_pinetree_top.png
  default_silver_sand.png
  default_mese_post_light_side.png
  default_mese_post_light_side_dark.png
  default_mese_post_light_top.png
  default_acacia_sapling.png
  default_acacia_bush_sapling.png
  default_acacia_bush_stem.png
  default_silver_sandstone.png -- Derived from a texture by GreenXenith (CC BY-SA 3.0)
  default_silver_sandstone_brick.png -- Derived from a texture by GreenXenith (CC BY-SA 3.0)
  default_silver_sandstone_block.png -- Derived from a texture by GreenXenith (CC BY-SA 3.0)
  default_acacia_bush_stem.png
  default_bush_stem.png
  default_pine_bush_stem.png
  default_desert_stone_brick.png
  default_sandstone_brick.png
  default_obsidian_brick.png
  default_desert_stone_brick.png
  default_sandstone_block.png
  default_obsidian_block.png
  default_stone_block.png
  default_desert_stone_block.png
  default_river_water.png
  default_river_water_source_animated.png
  default_river_water_flowing_animated.png
  default_stones.png -- Derived from a texture by sofar (CC0 1.0)
  default_stones_side.png -- Derived from a texture by sofar (CC0 1.0)
  default_fence_rail_acacia_wood.png
  default_fence_rail_aspen_wood.png -- Derived from a texture by sofar (CC BY-SA 3.0)
  default_fence_rail_junglewood.png

Good Morning Craft (CC BY-SA 4.0):
  default_dry_grass.png
  default_dry_grass_side.png
  default_rainforest_litter.png
  default_rainforest_litter_side.png

TumeniNodes (CC BY-SA 3.0):
  default_coniferous_litter.png
  default_coniferous_litter_side.png
  default_dry_dirt.png

Mirtilo (CC BY-SA 4.0):
  default_blueberries.png
  default_blueberry_overlay.png
  default_blueberry_bush_leaves.png
  default_blueberry_bush_sapling.png
  default_diamond.png
  default_diamond_block.png

Sounds
------

Glass breaking sounds (CC BY 3.0):
  1: http://www.freesound.org/people/cmusounddesign/sounds/71947/
  2: http://www.freesound.org/people/Tomlija/sounds/97669/
  3: http://www.freesound.org/people/lsprice/sounds/88808/

Mito551 (sounds) (CC BY-SA 3.0):
  default_glass_footstep.ogg

n3b http://opengameart.org/users/n3b (CC BY)
  player_damage.1.ogg
  player_damage.2.ogg

Tool breaking sounds
  default_tool_breaks - https://opengameart.org/content/35-wooden-crackshitsdestructions - CC0

Tool using sounds
  default_swoosh*.ogg - https://freesound.org/people/Wdfourtee/packs/12179/ - CC0

Chests sounds added by sofar, derived of several files mixed together:
  default_chest_open.ogg
  default_chest_close.ogg
    - http://www.freesound.org/people/Sevin7/sounds/269722/ CC0
    - http://www.freesound.org/people/Percy%20Duke/sounds/23448/ CC BY-3.0
    - http://www.freesound.org/people/kingsamas/sounds/135576/ CC BY-3.0
    - http://www.freesound.org/people/bulbastre/sounds/126887/ CC BY-3.0
    - http://www.freesound.org/people/Yoyodaman234/sounds/183541/ CC0

Metal sounds:
  default_dig_metal.ogg - yadronoff - CC BY-3.0
  - https://www.freesound.org/people/yadronoff/sounds/320397/
  default_dug_metal.*.ogg - Iwan Gabovitch - qubodup - CC0
  - http://opengameart.org/users/qubodup
  default_metal_footstep.*.ogg - (CC0 1.0) - CC0 1.0
  - https://freesound.org/people/mypantsfelldown/sounds/398937/
  default_place_node_metal.*.ogg - Ogrebane - CC0
  - http://opengameart.org/content/wood-and-metal-sound-effects-volume-2

Stone sounds:
  default_dig_cracky.*.ogg - Sn0wShepherd - CC BY-SA 4.0

Models
------
sofar (CC BY-SA 3.0):
  chest_open.obj
  torch_ceiling.obj
  torch_floor.obj
  torch_wall.obj

rudzik8 (CC BY-SA 4.0):
  default_sign.obj

Schematics
----------
paramat (CC BY-SA 3.0):
  acacia_tree.mts
  acacia_tree_from_sapling.mts
  apple_tree_from_sapling.mts
  aspen_tree.mts
  aspen_tree_from_sapling.mts
  emergent_jungle_tree.mts
  emergent_jungle_tree_from_sapling.mts
  jungle_tree_from_sapling.mts
  large_cactus.mts
  papyrus.mts
  pine_tree.mts
  pine_tree_from_sapling.mts
  snowy_pine_tree_from_sapling.mts
  small_pine_tree.mts
  small_pine_tree_from_sapling.mts
  snowy_small_pine_tree_from_sapling.mts

Shara RedCat (CC BY-SA 3.0):
  acacia_log.mts
  apple_log.mts
  aspen_log.mts
  jungle_log.mts
  pine_log.mts

TumeniNodes (CC BY-SA 3.0):
  pine_bush.mts

random-geek (CC BY-SA 3.0):
  blueberry_bush.mts

Casimir (CC BY-SA 3.0):
  conifertree*
  apple_tree.*
  jungle_tree.*
  tree*
  papyrus_with_roots.mts
