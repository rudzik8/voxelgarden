# `vg_alloy_furnace`

## License of source code

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

http://www.gnu.org/licenses/lgpl-2.1.html

* `init.lua` is a modified version of `default/furnace.lua` by celeron55 and
  contributors.

## License of media

Creative Commons Attribution-ShareAlike 4.0 (CC BY-SA 4.0)

http://creativecommons.org/licenses/by-sa/4.0/

* `vg_alloy_furnace*` by rudzik8 use work by Gambit (WTFPL) and Casimir
  (CC BY-SA 4.0).
