Visible Sneak
=============
A Minetest mod to make sneaking visible.

Originally by niwla23, adapted for Player API for Voxelgarden by rudzik8.

* `character_sneak.b3d` by stujones11 and An0n3m0us (CC BY-SA 3.0)
