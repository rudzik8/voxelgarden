player_api.register_model("character_sneak.b3d", {
	animation_speed = 10,
	textures = {"character.png"},
	visual_size = {x = 1, y = 0.9},
	animations = {
		-- Standard animations.
		stand     = {x = 0,   y = 79},
		lay       = {x = 162, y = 166, eye_height = 0.3, override_local = true,
			collisionbox = {-0.6, 0.0, -0.6, 0.6, 0.3, 0.6}},
		walk      = {x = 168, y = 187},
		mine      = {x = 189, y = 198},
		walk_mine = {x = 200, y = 219},
		sit       = {x = 81,  y = 160, eye_height = 0.8, override_local = true,
			collisionbox = {-0.3, 0.0, -0.3, 0.3, 1.0, 0.3}}
	},
	collisionbox = {-0.3, 0.0, -0.3, 0.3, 1.6, 0.3},
	stepheight = 0.6,
	eye_height = 1.47,
})

controls.register_on_press(function(player, key)
	if key == "sneak" then
		player_api.set_model(player, "character_sneak.b3d")
	end
end)

controls.register_on_release(function(player, key)
	if key == "sneak" then
		player_api.set_model(player, "character.b3d")
	end
end)
