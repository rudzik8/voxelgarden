vg_score = {
	update = function()
		return
	end
}

if minetest.settings:get_bool("enable_damage") then

local S = minetest.get_translator("vg_score")

local score_hud = {}

local function add_hud(player)
	local name = player:get_player_name()
	local meta = player:get_meta()
	if meta:get_int("show_score") == 0 then
		score_hud[name] = player:hud_add({
			type = "text",
			position = {x=0.5, y=1},
			offset = {x = 0, y = -73},
			name = "score",
			text = minetest.colorize("yellow", meta:get_int("score")),
			z_index = 99,
		})
	end
end

minetest.register_on_joinplayer(add_hud)

minetest.register_on_leaveplayer(function(player)
	local name = player:get_player_name()
	player:hud_remove(score_hud[name])
	score_hud[name] = nil
end)

-- Update the score
-- (amount is +1 by default)
local function update_score(player, amount)
	if not player then return end
	if not amount then amount = 1 end
	local name = player:get_player_name()
	local meta = player:get_meta()
	local score = meta:get_int("score")
	if score >= 9999999999999 then return end -- cheater >:O
	score = score + amount
	meta:set_int("score", score)
	if score_hud[name] then
		if meta:get_int("show_score") == 1 then
			player:hud_remove(score_hud[name])
			score_hud[name] = nil
			return
		end
		player:hud_change(score_hud[name], "text", minetest.colorize("yellow", score))
	else
		add_hud(player)
	end
end

minetest.register_on_placenode(function(pos, newnode, placer)
	update_score(placer)
end)

minetest.register_on_dignode(function(pos, oldnode, digger)
	update_score(digger)
end)

minetest.register_on_player_hpchange(function(player, hp_change)
	-- Don't add score on healing
	if hp_change > 0 then return end
	update_score(player, hp_change)
end)

minetest.register_on_dieplayer(function(player)
	update_score(player, -20)
end)

minetest.register_on_chat_message(function(name)
	update_score(minetest.get_player_by_name(name))
end)

minetest.register_on_chatcommand(function(name, command)
	-- Don't add score when checking score, version or privs
	if command == "score" or command == "ver" or command == "privs" then
		return
	end
	update_score(minetest.get_player_by_name(name))
end)

minetest.register_on_craft(function(itemstack, player)
	update_score(player)
end)

minetest.register_on_player_inventory_action(function(player)
	update_score(player)
end)

minetest.register_on_protection_violation(function(pos, name)
	update_score(minetest.get_player_by_name(name), -5)
end)

minetest.register_on_item_pickup(function(itemstack, picker)
	update_score(picker)
end)

-- Show score command
-- When called without a playername, caller's name is used
minetest.register_chatcommand("score", {
	description = S("Show score"),
	func = function(name, param)
		local pname = param
		local mode = "peek"
		if pname == "" then
			pname = name
			mode = "toggle"
		end

		local player = minetest.get_player_by_name(pname)
		if not player then
			return false, S("Invalid or offline player!")
		end
		local meta = player:get_meta()

		if mode == "peek" then
			return true, S("@1's score: @2", pname, minetest.colorize("yellow", meta:get_int("score")))
		elseif mode == "toggle" then
			local new_int, msg
			if meta:get_int("show_score") == 0 then
				new_int = 1
				msg = S("Showing score is set to OFF")
			else
				new_int = 0
				msg = S("Showing score is set to ON")
			end
			meta:set_int("show_score", new_int)
			update_score(player, 0)
			return true, msg
		end
	end
})

-- Expose the update_score function
vg_score.update = update_score

end
