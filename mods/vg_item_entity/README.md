# `vg_item_entity`

This mod was made specially for Voxelgarden and overrides the builtin item
entity to implement new features. These include:

* Destroy items on contact with some nodes (`_destroys_items = true`),
  like lava or fire.
* Do not destroy items that way if they have `indestructible_item` group.
* Only destroy items based on a specific group (`_destroys_group = "group"`),
  like `"flammable"` for fire.
* Merge stacks so that the resulting stack is placed between the merging ones,
  and not in place of one of them. (from [builtin_item](https://codeberg.org/tenplus1/builtin_item))


## License of source code

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

http://www.gnu.org/licenses/lgpl-2.1.html
