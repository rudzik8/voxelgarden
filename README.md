Voxelgarden
===========

[![ContentDB](https://content.minetest.net/packages/Casimir/voxelgarden/shields/downloads/)](https://content.minetest.net/packages/Casimir/voxelgarden/)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-4baaaa.svg)](CODE_OF_CONDUCT.md)

A classic game for the Minetest engine, to explore, survive and build.

Compatible with most mods for Minetest Game.

![Screenshot](screenshot.png "Screenshot")


Getting Started
---------------

Open your inventory and take a look at the **craftguide** in the top left.

You need to find crumbled stone to craft your first stone tools.

To get sticks, place 3 leaves vertically on the crafting grid.


Features
--------

* "Retro" 2D mobs, inspired by Minetest 0.3.
* Good textures by default, forming an overall consistent style.
* Game mechanics different from minetest_game and Minecraft, giving the game it's unique feeling.
* Self-planting saplings. No need to collect them. Forests will spread naturally.
* Progressive gameplay, while still offering different ways to achieve your goals.
* Coal-powered alloy furnaces that make ingots that are unobtainable through regular cooking or crafting.
* More realistic metal progression. Iron lumps make iron ingots; alloy iron with coal to make steel.
* Hellish Nether realm, separated by the regular world with an unbreakable bedrock layer.
* Nether and Floatlands (v7) portals, ignited with the Realm Striker, an item found within dungeons deep underground.
* Not only stairs and slabs, but a wide range of sub-nodes, enabling detailed buildings.
* Most items can be placed down, allowing for interesting decorative elements.
* Dyes can be placed on wool and similar nodes to instantly color them.
* Must-see mapgen. Getting the best out of mapgen v6 and v7.
* MC-like hunger and healing.
* Sprint.
* Simple score system, motivating players to not abandon their worlds.
* Modular doors, using which you can build more than just doors. e.g. window frames, hatches, street lamps...
* Cactus has spikes and they hurt.
* Details, details, details. Don't miss them.


Contribute
----------

Issues use the system of:

    bugs >> cleanups >> features >> nice to haves

Which means bugs should be done first, then cleanups, then new features, then the things nice to have. Every issue should be labeled with one of those.

This has the downside of having only few features, but the upside of having a stable and mostly bug free base to work with and little maintenance. I prefer to have a few well done features working over many not working.

Bigger additions should work as a mod on their own. This allows easier testing, benefits a modular design and ensure that they can still be used as a mod if the feature does not get merged.

See [CONTRIBUTING.md](CONTRIBUTING.md) for instructions.


Goals/aesthetics/concept
------------------------

Voxelgarden is meant as a survival and building game, complete with all basic features one might expect. All storyline, lore and specialized behavior should be kept out of it. Eventually there might be a separate modpack for the story of Voxelgarden. Think of it as the physical world in which different cultures can life in different ages having different stories playing out.

It is meant to be compatible with mods written for Minetest Game and therefor includes a lot of content from MTG when several mods depend on it. Likewise VG is also meant as a basic platform for mods, but also as a complete game in itself.

The game should offer a continuous progression from "stone age" technology to basic machines. Further more complex technology is left to mods like mesecons.  
It takes the aesthetics that make Minetest different from "other open world voxel mining and crafting games" and maybe a romanticized retro gaming look.

What not to add:

* Anything that is just decoration. New content should always have a personality, some unique behavior and properties. There is no use in adding a new tree type if it only differs in shape and textures.
* Exploding complexity. Think about traffic regulations. They are a set of rules easy enough for a human to learn, but the code needed to run traffic lights exceeds the volume of traffic regulations by orders of magnitude. If we can build roads without traffic light, we should.
* Anything that is not easy to maintain. I personally can't Blender, and I don't care enough to learn. That's why there are only 2D entities.

Voxelgarden should be (in that order):

    a complete game >> easy to maintain >> compatible with minetest game


License of source code
----------------------

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU Lesser General Public License as published by
the Free Software Foundation; either version 2.1 of the License, or
(at your option) any later version.

http://www.gnu.org/licenses/lgpl-2.1.html

For every mod applies the original license. See list below.
All other media is licenced under Creative Commons BY-SA license.

bedrock2 (MIT), beds (LGPL), bones (MIT), bucket (LGPL), conifer (LGPL), controls (MIT), default (LGPL), doors (LGPL), dye (LGPL), game_commands (MIT), headanim (MIT), farming (LGPL), fire (LGPL), flowers (LGPL), footsteps (LGPL), mobs (MIT), mobs\_flat (LGPL), mtg_craftguide (MIT), nether (ISC), physics (LGPL), placeitem (LGPL), player_api (LGPL), playerphysics (MIT), show_wielded_item (LGPL), sprint (CC0), stairs (LGPL), stairsplus (zlib/libpng), tt (MIT), tt_base (MIT), vg_alloy (LGPL), vg_alloy_furnace (LGPL), vg_formspec (LGPL), vg_hunger (LGPL), vg_init (LGPL), vg_item_entity (LGPL), vg_mapgen (LGPL), vg_score (LGPL), vignette (LGPL), visible_sneak (MIT), walls (LGPL) wool (LGPL), creative (MIT), xpanes (LGPL)

Mods equivalent to and mirroring Minetest Game:
binoculars (MIT), boats (MIT) (except burntime), carts (MIT), dungeon_loot (MIT), env_sounds (MIT), fireflies (MIT) (except some textures), map (MIT), screwdriver (LGPL) (except texture), sethome (MIT), sfinv (MIT), tnt (MIT) (except textures), vessels (LGPL) (except textures), weather (MIT)
